def func(l):
    list1 = sorted(list(filter(lambda x : x[0] == 'x', l)))
    list2 = sorted(list(filter(lambda x : x[0] != 'x', l)))

    return list1 + list2


l1 = ['bbb', 'ccc', 'axx', 'xzz', 'xaa']
l2 = ['mix', 'xyz', 'apple', 'xanadu', 'aardvark']

print(func(l1))
print(func(l2))
