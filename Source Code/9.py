import re

try:
    with open('router_log.txt', 'r') as fptr:
        router_log = fptr.read()

        # print(router_log)

        interface_exp = '[a-zA-Z]+[0-9]/[0-9]'
        interface = re.findall(interface_exp, router_log)

        method_status = '(manual up|unset)'
        status = re.findall(method_status, router_log)

        for i in range(len(interface)):
            print(interface[i],',', status[i])

except FileNotFoundError:
    print('File is Removed!')
