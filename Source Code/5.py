def unique(l):
    return list(set(l))


l1 = [1, 2, 2, 3]
l2 = [2, 2, 3, 3, 3]

print(unique(l1))
print(unique(l2))
