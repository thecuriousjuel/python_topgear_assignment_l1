def func_count(l):

    return len(list(filter(lambda x : x[0]==x[-1],filter(lambda x: len(x) >= 2, l))))


l1 = ['axa', 'xyz', 'gg', 'x', 'yyy']
l2 = ['x', 'cd', 'cnc', 'kk']
l3 = ['bab', 'ce', 'cba', 'syanora']

print(func_count(l1))
print(func_count(l2))
print(func_count(l3))